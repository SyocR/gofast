package latency

import "os"
import "log"
import "time"
import "net/http"
import "encoding/json"
import "net/http/httptrace"
import "github.com/influxdata/tdigest"
import "gitlab.com/SyocR/gofast/fastcom"

type latencyResults struct {
	TestType     string        `json:"Test Type"`
	TestStart    string        `json:"Start timestamp"`
	TestDuration time.Duration `json:"Duration timestamp"`
	Perc_50      float64       `json:"50th percentile"`
	Perc_75      float64       `json:"75th percentile"`
	Perc_90      float64       `json:"90th percentile"`
	Perc_99      float64       `json:"99th percentile"`
}

func Measure(count int) (res latencyResults) {
	start := time.Now()
	deltas := make([]time.Duration, count)
	urlRange := uint64(0) // bytes to download/upload from URL
	urls := fastcom.GetUrls(count, urlRange)
	// This will test latency for a http connection to be established
	// not ping. Expect higher numbers than you might be used to.
	td := tdigest.NewWithCompression(100) // for percentile stats

	for i, url := range urls {
		//fmt.Printf("Round %d in loop\n", i)
		start := time.Now()
		trace := &httptrace.ClientTrace{
			GotConn: func(conInfo httptrace.GotConnInfo) {
				// calculate delta once http connection is established
				deltas[i] = time.Since(start)
			},
		}
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			log.Printf("WARNING: Failed to create request object for url %s with error %s", url, err)
		}
		// Create request object with the trace context
		req = req.WithContext(httptrace.WithClientTrace(req.Context(), trace))
		_, err = http.DefaultTransport.RoundTrip(req)
		if err != nil {
			log.Printf("WARNING: Failed to make http(s) roundtrip for url %s with error %s", url, err)
		}
		deltas[i] = time.Since(start)
	}

	for _, delta := range deltas {
		//fmt.Printf("Delta: %d\n", delta)
		deltaMilliseconds := float64(delta) / float64(time.Millisecond)
		//fmt.Printf("Delta in ms: %d\n", deltaMilliseconds)
		td.Add(deltaMilliseconds, 1)
	}
	timeText, _ := start.MarshalText()
	testTime := time.Since(start)
	res = latencyResults{
		TestType:     "Latency",
		TestStart:    string(timeText),
		TestDuration: testTime,
		Perc_50:      td.Quantile(0.5),
		Perc_75:      td.Quantile(0.75),
		Perc_90:      td.Quantile(0.90),
		Perc_99:      td.Quantile(0.99),
	}

	return res
}

func Output(res latencyResults, jsonOutput bool, influxdb bool, path string) (err error) {
	if jsonOutput {
		jsonRes, err := json.Marshal(res)
		if err != nil {
			return err
		}
		f, err := os.OpenFile(path+"/gofast.json", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		defer f.Close()
		if err != nil {
			return err
		}
		_, err = f.Write(append(jsonRes, byte(0x0a)))
		if err != nil {
			return err
		}
	} else if influxdb {
		// influxdb line protocol
		log.SetFlags(0)
		log.SetOutput(os.Stdout)
		log.Printf("speed-latency 50th-percentile=%f,75th-percentile=%f,90th-percentile=%f,99th-percentile=%f,ns-duration=%d %d", res.Perc_50, res.Perc_75, res.Perc_90, res.Perc_99, res.TestDuration.Nanoseconds(), time.Now().UnixNano())
		log.SetOutput(os.Stderr)
		log.SetFlags(1)
	} else {
		log.Printf("Latency 50th percentile %.2f ms\n", res.Perc_50)
		log.Printf("75th percentile %.2f ms\n", res.Perc_75)
		log.Printf("90th percentile %.2f ms\n", res.Perc_90)
		log.Printf("99th percentile %.2f ms\n", res.Perc_99)
		log.Printf("Latency test took %.2f seconds\n", res.TestDuration.Seconds())
		log.Printf("Latency test start time: %s\n", res.TestStart)
	}

	return
}
