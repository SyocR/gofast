package main

import "fmt"
import "log"
import "os"
import "flag"
import "gitlab.com/SyocR/gofast/latency"
import "gitlab.com/SyocR/gofast/bandwidth"

func validatePath(path string) (err error) {
	fi, err := os.Stat(path)
	if path == "" {
		err := fmt.Errorf("Setting the --json flag also requires setting the --path")
		return err
	}
	if os.IsNotExist(err) {
		err := fmt.Errorf("Path %s set by --filepath must point to a directory", path)
		return err
	}
	switch mode := fi.Mode(); {
	case mode.IsRegular():
		err := fmt.Errorf("Path %s set by --filepath must point to a directory", path)
		return err
	case mode.IsDir():
		return nil
	}
	return
}

func main() {
	jsonOutput := flag.Bool("json", false, "Print ouput as json instead of using stdout")
	influxdb := flag.Bool("influx", false, "Print ouput as influx line protocol")
	filePath := flag.String("path", "", "Directory path to write json output")
	bytesToDownload := flag.Uint64("download", 3000000, "Bytes to download")
	bytesToUpload := flag.Uint64("upload", 1000000, "Bytes to upload")
	latencyCount := flag.Int("conn-count", 100, "Connections to make for latency test")
	flag.Parse()

	if *jsonOutput {
		err := validatePath(*filePath)
		if err != nil {
			log.Fatalf("ERROR: failed parsing path. %s\n", err)
		}
	}
	latRes := latency.Measure(*latencyCount)
	err := latency.Output(latRes, *jsonOutput, *influxdb, *filePath)
	if err != nil {
		log.Fatalf("ERROR: Failed during output. %s\n", err)
	}
	downRes, err := bandwidth.Measure(string("download"), *bytesToDownload)
	if err != nil {
		log.Printf("WARNING: Download test returned error %s\n", err)
	}
	err = bandwidth.Output(downRes, *jsonOutput, *influxdb, *filePath)
	if err != nil {
		log.Fatalf("ERROR: Failed during output. %s\n", err)
	}
	upRes, err := bandwidth.Measure(string("upload"), *bytesToUpload)
	if err != nil {
		log.Printf("WARNING: Upload test returned error %s\n", err)
	}
	err = bandwidth.Output(upRes, *jsonOutput, *influxdb, *filePath)
	if err != nil {
		log.Fatalf("ERROR: Failed during output. %s\n", err)
	}
}
