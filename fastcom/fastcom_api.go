package fastcom

import "io"
import "log"
import "fmt"
import "regexp"
import "bytes"
import "strconv"
import "net/url"
import "net/http"
import "encoding/json"

func rangemodUrls(urls []string, bytes uint64) (modifiedUrls []string) {
	// URLs from the API does not contain this '/range/0-X/' part
	// This is what the .js fast.com version does as well
	// X are bytes to send or recieve. Latency test thus has 0-0
	for _, targetUrl := range urls {
		u, err := url.Parse(targetUrl)
		if err != nil {
			log.Fatalf("Failed to parse url %s with error %s", targetUrl, err)
		}
		//fmt.Printf("Target url %s u.Path %s", targetUrl, u.Path)
		bytes := strconv.FormatUint(bytes, 10) // base 10
		u.Path = u.Path + "/range/0-" + bytes
		modifiedUrls = append(modifiedUrls, u.String())
	}
	return
}

func GetUrls(urlCount int, bytes uint64) (urls []string) {
	token, err := getFastToken()
	if err != nil {
		log.Printf("ERROR: Failed to get token with error %s\n", err)
	}

	for len(urls) < urlCount {
		url := fmt.Sprintf("https://api.fast.com/netflix/speedtest/v2/?https=https&token=%s&urlCount=%d", token, urlCount-len(urls))

		jsonData, err := getPage(url)
		if err != nil {
			log.Printf("WARNING: Failed to get urls from %s with error %s\n", url, err)
		}

		type Location struct {
			Country string
			City    string
		}
		type Client struct {
			Asn string
			Isp string
			Loc Location `json:"location"`
		}
		type Targets struct {
			Url  string
			Loc  Location `json:"location"`
			Name string
		}
		type ApiResponse struct {
			Client  Client
			Targets []Targets
		}

		var jsnResp ApiResponse

		json.Unmarshal([]byte(jsonData), &jsnResp)

		for _, target := range jsnResp.Targets {
			urls = append(urls, target.Url)
		}
		urls = rangemodUrls(urls, bytes)
	}

	return
}

func getFastToken() (token string, err error) {
	baseURL := "https://fast.com"
	fastBody, err := getPage(baseURL)
	if err != nil {
		return "", err
	}
	// Extract the app script url
	re := regexp.MustCompile("app-.*\\.js")
	scriptNames := re.FindAllString(fastBody, 1)
	scriptURL, _ := url.Parse(baseURL)
	scriptURL.Path = scriptURL.Path + scriptNames[0]
	// Extract the token
	scriptBody, err := getPage(scriptURL.String())
	if err != nil {
		return "", err
	}
	re = regexp.MustCompile("token:\"[[:alpha:]]*\"")
	tokens := re.FindAllString(scriptBody, 1)

	if len(tokens) > 0 {
		token = tokens[0][7 : len(tokens[0])-1]
	} else {
		err := fmt.Errorf("Failed to get fast.com API token")
		return "", err
	}

	return
}

func getPage(url string) (contents string, err error) {
	// Create the string buffer
	buffer := bytes.NewBuffer(nil)
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return contents, err
	}
	defer resp.Body.Close()
	// Write the body to file
	_, err = io.Copy(buffer, resp.Body)
	if err != nil {
		return contents, err
	}
	contents = buffer.String()

	return
}
