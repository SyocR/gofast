package bandwidth

import "os"
import "io"
import "log"
import "time"
import "bytes"
import "net/http"
import "encoding/json"
import "gitlab.com/SyocR/gofast/fastcom"

func Output(res bandwidthRes, jsonOutput bool, influxdb bool, path string) (err error) {
	if jsonOutput {
		jsonRes, err := json.Marshal(res)
		if err != nil {
			return err
		}
		f, err := os.OpenFile(path+"/gofast.json", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		defer f.Close()
		if err != nil {
			return err
		}
		_, err = f.Write(append(jsonRes, byte(0x0a)))
		if err != nil {
			return err
		}
	} else if influxdb {
		// infludb line protocol
		log.SetFlags(0)
		log.SetOutput(os.Stdout)
		log.Printf("speed-%s bits/s=%f,ns-duration=%d %d", res.TestType, res.BitsPerSecond, res.TestDuration.Nanoseconds(), time.Now().UnixNano())
		log.SetOutput(os.Stderr)
		log.SetFlags(1)
	} else {
		log.Printf("Results for %s test", res.TestType)
		log.Printf("Throughput was %.2f bits per second", res.BitsPerSecond)
		log.Printf("Test took %.2f seconds\n", res.TestDuration.Seconds())
		log.Printf("Test start time: %s\n", res.TestStart)
	}

	return
}

type transferStatus struct {
	bytes uint64
	err   error
}

type bandwidthRes struct {
	TestType      string        `json:"Test Type"`
	TestStart     string        `json:"Start timestamp"`
	TestDuration  time.Duration `json:"Duration timestamp"`
	BitsPerSecond float64       `json:"bits per second`
}

func Measure(task string, bytes uint64) (res bandwidthRes, err error) {
	count := int(bytes / 25000000) // fast.com returns no more than 25MB per url
	transfered := uint64(0)
	if count <= 5 {
		count = 5
	}
	urlBytes := bytes / uint64(count)
	urls := fastcom.GetUrls(count, urlBytes)
	result := make([]transferStatus, len(urls))
	c := make(chan transferStatus)

	//fmt.Printf("URLs retrieved: %s\n", urls)
	start := time.Now()
	for _, url := range urls {
		//fmt.Printf("url to process: %s\n", url)
		switch task {
		case "download":
			go downloadUrls(url, c)
		case "upload":
			go uploadUrls(url, urlBytes, c)
		}
	}
	for i, _ := range result { // loop ensures all go routines have completed
		result[i] = <-c
		transfered += result[i].bytes
	}
	delta := time.Since(start)
	deltaSeconds := float64(delta) / float64(time.Second)
	bps := float64(transfered*8) / deltaSeconds // *8 to get bits from bytes
	//fmt.Printf("Bytes transfered %d seconds %.2f\n", transfered, deltaSeconds)
	//fmt.Println(delta)

	timeText, _ := start.MarshalText()
	res = bandwidthRes{
		TestType:      task,
		TestStart:     string(timeText),
		TestDuration:  delta, // nanosecond count
		BitsPerSecond: bps,
	}

	return
}

func uploadUrls(url string, bytesToSend uint64, c chan transferStatus) {
	client := &http.Client{}
	buff := bytes.NewBuffer(nil)
	data := make([]byte, bytesToSend)
	r := bytes.NewReader(data)

	request, err := http.NewRequest("POST", url, r)
	if err != nil {
		log.Printf("WARNING: Error creating request object. url %s error %s\n", url, err)
	}
	request.Header.Set("Content-type", "application/octet-stream")
	response, err := client.Do(request)
	if err != nil {
		log.Printf("Error making request. url %s error %s\n", url, err)
	}
	io.Copy(buff, response.Body) // this copy is required so we don't return too early
	//fmt.Printf("Uploaded %d to url %s\n", bytesToSend, url)
	c <- transferStatus{uint64(bytesToSend), err}

	defer response.Body.Close()
}

func downloadUrls(url string, c chan transferStatus) {
	client := &http.Client{}
	buff := bytes.NewBuffer(nil)

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("WARNING: Error creating request object. url %s error %s\n", url, err)
	}
	response, err := client.Do(request)
	if err != nil {
		log.Printf("Error making request. url %s error %s\n", url, err)
	}
	n, _ := io.Copy(buff, response.Body)
	//fmt.Printf("Downloaded %d form url %s\n", n, url)
	c <- transferStatus{uint64(n), err}

	defer response.Body.Close()
}
