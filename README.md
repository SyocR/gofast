# gofast

This is an alternative to speedtest.net cli using the undocumented API of fast.com

Main features are
 * json output
 * influx line protocol output
 * individual configs for download, upload and latency tests
 * Highly performant
 * Uses fast.com v2 API

This program will not print output while running. Only summarized stats on test completion.
 
 Heavily inspired by https://github.com/gesquive/fast-cli

 I wrote this as a go learining project. Code quality reflects that.
 
 ## Usage
 
 ```
 ./gofast --help
Usage of ./gofast:
  -conn-count int
        Connections to make for latency test (default 100)
  -download uint
        Bytes to download (default 3000000)
  -influx
        Print ouput as influx line protocol
  -json
        Print ouput as json instead of using stdout
  -path string
        Directory path to write json output
  -upload uint
        Bytes to upload (default 1000000)
 ```
 
 ## Sample telegraf config
 
 This is how I execute this program using telegraf and influxdb
 
 ```
 [[inputs.exec]]
   ## Commands array
   commands = ["/gofast --conn-count 50 --download 5000000 --upload 3000000 --influx"]

   ## Timeout for each command to complete.
   timeout = "1m"

   interval = "30m"

   ## measurement name suffix (for separating different commands)
   #name_suffix = "_mycollector"

   ## Data format to consume.
   ## Each data format has its own unique set of configuration options, read
   ## more about them here:
   ## https://github.com/influxdata/telegraf/blob/master/docs/DATA_FORMATS_INPUT.md
   data_format = "influx"
```
